    /*----------------------------------------
                        Preloader
    ------------------------------------------*/
    $('.js-preloader').preloadinator({
        minTime: 2000,
        scroll: false

    });

    $(document).ready(function() {
    

        /*------------------------------------------
                 Scroll bottom navigation
        --------------------------------------------*/
        $('.action_btn').on('click', function() {

            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 600);
                    return false;
                }
            }
        })

        /*----------------------------------------
          Scroll to top
  ----------------------------------------*/
        function BackToTop() {
            $('.scrolltotop').on('click', function() {
                $('html, body').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });

            $(document).scroll(function() {
                var y = $(this).scrollTop();
                if (y > 600) {
                    $('.scrolltotop').fadeIn();
                } else {
                    $('.scrolltotop').fadeOut();
                }
            });
            $(document).scroll(function() {
                var m = $(this).scrollTop();
                if (m > 400) {
                    $('.chat-popup').fadeIn();
                } else {
                    $('.chat-popup').fadeOut();
                }
            });
        }
        BackToTop();




        /*-------------------------------------------------*/
        /*    scroll between sections
        /*-------------------------------------------------*/

        // Add scrollspy to <body>
        $('body').scrollspy({
            target: ".list_menu",
            offset: 50
        });

        // Add smooth scrolling on all links inside the navbar
        $(".site-menu a").on('click', function(event) {
            if (this.hash !== "") {
                event.preventDefault();

                var hash = this.hash;


                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function() {

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });

        // $('.list-details-tab li, .hero_list li').on('click', (function() {
        //     $('li').removeClass("active");
        //     $(this).addClass("active");
        // }));


        /* ----------------------------------------
              Hide Show Header on Scroll
        ------------------------------------------ */
        function HideShowHeader() {

            var didScroll;
            var lastScrollTop = 0;
            var delta = 50;
            var navbarHeight = 75;
            var navbarHideAfter = navbarHeight

            $(window).scroll(function(event) {
                didScroll = true;
            });

            if ($('.scroll-hide').length > 0) {

                setInterval(function() {
                    if (didScroll) {
                        hasScrolled();
                        didScroll = false;
                    }
                }, 100);
            }
            return false;

            function hasScrolled() {
                var st = $(this).scrollTop();

                if (Math.abs(lastScrollTop - st) <= delta)
                    return;

                if (st > lastScrollTop && st > navbarHideAfter) {
                    if ($('.scroll-hide').length > 0) {
                        $('header').addClass('hide');
                    }
                } else {
                    if ($('.scroll-hide').length > 0) {
                        if (st + $(window).height() < $(document).height()) {
                            $('header').removeClass('hide');
                            $('.header.transparent').addClass('scroll');
                        }
                    }

                    if ($(window).scrollTop() < 300) {
                        $('.header.transparent').removeClass('scroll');
                    }
                }

                lastScrollTop = st;
            }
        }
        HideShowHeader();




        /* ----------------------------------------
              Counter animation
        ------------------------------------------*/
        $('.counter-text').appear(function() {
            var element = $(this);
            var timeSet = setTimeout(function() {
                if (element.hasClass('counter-text')) {
                    element.find('.counter-value').countTo();
                }
            });
        });

        /*--------------------------------------------
                           Video Player
         --------------------------------------------*/
        $(".player").mb_YTPlayer({
            containment: '#video-wrapper',
            mute: true,
            autoplay: true,
            showControls: false,
            quality: 'hd720'
        });


        jQuery(document).ready(function($) {
            "use strict";


            /*-------------------------------------
                    Magnific Popup js
            --------------------------------------*/
            $('.popup-yt, .property-yt').magnificPopup({
                type: 'iframe',
                mainClass: 'mfp-fade',
                preloader: true,
            });

            $('a.btn-gallery').on('click', function(event) {
                event.preventDefault();

                var gallery = $(this).attr('href');

                $(gallery).magnificPopup({
                    delegate: 'a',
                    type: 'image',
                    gallery: {
                        enabled: true
                    }
                }).magnificPopup('open');
            });

            /* -------------------------------------
                  Footer Accordion
            -------------------------------------- */
            $(".nav-folderized h2").on('click', (function() {
                $(this).parent(".nav").toggleClass("open");
                $('html, body').animate({
                    scrollTop: $(this).offset().top - 170
                }, 1500);
            }));

            /* -------------------------------------
                    Responsive menu
            -------------------------------------- */
            var siteMenuClone = function() {

                $('.js-clone-nav').each(function() {
                    var $this = $(this);
                    $this.clone().attr('class', 'site-nav-wrap').appendTo('.site-mobile-menu-body');
                });

                setTimeout(function() {

                    var counter = 0;
                    $('.site-mobile-menu .has-children').each(function() {
                        var $this = $(this);

                        $this.prepend('<span class="arrow-collapse collapsed">');

                        $this.find('.arrow-collapse').attr({
                            'data-toggle': 'collapse',
                            'data-target': '#collapseItem' + counter,
                        });

                        $this.find('> ul').attr({
                            'class': 'collapse',
                            'id': 'collapseItem' + counter,
                        });

                        counter++;

                    });

                }, 1000);

                $('body').on('click', '.js-menu-toggle', function(e) {
                    var $this = $(this);
                    e.preventDefault();

                    if ($('body').hasClass('offcanvas-menu')) {
                        $('body').removeClass('offcanvas-menu');
                        $this.removeClass('active');
                    } else {
                        $('body').addClass('offcanvas-menu');
                        $this.addClass('active');
                    }
                })

            };
            siteMenuClone();



            /* -------------------------------------
                     Category menu Activation
            -------------------------------------- */


            // $(".chat-popup, .chat-close").click(function () {

            //     $(".agent-chat-form").toggle();

            // });

            // $(".dropdown-filter").on('click', function () {

            //     $(".explore__form-checkbox-list").toggleClass("filter-block");

            // });
            /* -------------------------------------
                       Slider
            -------------------------------------- */
            //Trending place slider
            var trending_place = new Swiper('.trending-place-wrap', {
                slidesPerView: 3,
                spaceBetween: 30,
                speed: 1500,
                loop: true,
                pagination: {
                    el: '.trending-pagination',
                    clickable: true,
                },
                // Responsive breakpoints
                breakpoints: {
                    767: {
                        slidesPerView: 1,
                        slidesPerGroup: 1,
                    },
                    1199: {
                        slidesPerView: 2,
                        spaceBetween: 30
                    }
                }
            });

            //Partner slider
            var partner_slider = new Swiper('.partner-wrap', {
                slidesPerView: 4,
                spaceBetween: 30,
                loop: true,
                speed: 1000,
                autoplay: {
                    delay: 2500,
                    disableOnInteraction: false,
                },
                navigation: {
                    nextEl: '.partner_next',
                    prevEl: '.partner_prev',
                },
                // Responsive breakpoints
                breakpoints: {

                    575: {
                        slidesPerView: 2,
                        spaceBetween: 30
                    },
                    767: {
                        slidesPerView: 3,
                        spaceBetween: 30
                    },
                    991: {
                        slidesPerView: 4,
                        spaceBetween: 30
                    }
                }
            });






        });





    });